/**
 * @file <spiri3d_loop_functions.h>
 *
 * @author Daniel H. Stolfi
 *
 * ADARS project -- PCOG / SnT / University of Luxembourg
 */

#ifndef SPIRI3D_LOOP_FUNCTIONS_H_
#define SPIRI3D_LOOP_FUNCTIONS_H_

#include <argos3/core/simulator/loop_functions.h>

using namespace argos;

const Real END_THRESHOLD = 0.00001;
const int MAX_ROBOTS = 31;
const int MAX_HISTORY = 1000;

class CSpiri3DLoopFunctions : public CLoopFunctions {

public:
   typedef std::array<CVector3, MAX_ROBOTS> CPrevious;

   typedef std::array<Real, MAX_HISTORY> CDistances;
   typedef std::array<CDistances, MAX_ROBOTS> CHistory;


   CSpiri3DLoopFunctions();
   virtual ~CSpiri3DLoopFunctions() {}
   virtual void Init(TConfigurationNode& t_tree);
   virtual void PostStep();
   virtual void PostExperiment();

private:
   std::vector<int> m_cFailure;
   CHistory m_cHistory;
   CPrevious m_cPrevious;
   float m_drobot_previous;
   unsigned m_counter, m_iterations, m_maxiterations;
   bool m_trace;
};


#endif
