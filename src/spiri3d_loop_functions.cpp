/**
 * @file <spiri3d_loop_functions.cpp>
 *
 * @author Daniel H. Stolfi
 *
 * ADARS project -- PCOG / SnT / University of Luxembourg
 */

#include "spiri3d_loop_functions.h"

#include <argos3/core/simulator/loop_functions.h>
#include <argos3/plugins/robots/spiri/simulator/spiri_entity.h>
#include <math.h>

CSpiri3DLoopFunctions::CSpiri3DLoopFunctions() :
      m_drobot_previous(0.0), m_counter(0), m_iterations(100), m_trace(false), m_maxiterations(1000) {
}

void CSpiri3DLoopFunctions::Init(TConfigurationNode &t_tree) {
   try {
      char buffer[50];
      TConfigurationNodeIterator itParam;
      for (itParam = itParam.begin(&t_tree); itParam != itParam.end(); ++itParam) {

         TConfigurationNode &tParam = *itParam;
         if (itParam->Value() == "params") {
            GetNodeAttributeOrDefault(tParam, "iterations", m_iterations, m_iterations);
            GetNodeAttributeOrDefault(tParam, "max-iterations", m_maxiterations, m_maxiterations);
            GetNodeAttributeOrDefault(tParam, "trace", m_trace, m_trace);
         }
         if (itParam->Value() == "failure") {
            for (int i = 0; i <= MAX_ROBOTS; ++i) {
               int failure_time = 0;
               sprintf(buffer, "robot%d", i);
               GetNodeAttributeOrDefault(tParam, buffer, failure_time, failure_time);
               m_cFailure.push_back(failure_time);
            }
         }
      }
      for (int i = 0; i < MAX_ROBOTS; ++i) {
         m_cPrevious[i] = CVector3::ZERO;
         for (int j = 0; j < MAX_HISTORY; ++j) {
            m_cHistory[i][j] = 0.0;
         }
      }


   } catch (CARGoSException &ex) {
      THROW_ARGOSEXCEPTION_NESTED("Error initialising the loop functions", ex);
   }
}

void CSpiri3DLoopFunctions::PostStep() {
   CSpace::TMapPerType &cSpiribots = GetSpace().GetEntitiesByType("spiri");

   // Ghost
   CVector3 cPosGhost = CVector3::ZERO;
   for (CSpace::TMapPerType::iterator it1 = cSpiribots.begin(); it1 != cSpiribots.end(); ++it1) {
      CSpiriEntity &cSpiriBot1 = *any_cast<CSpiriEntity*>(it1->second);

      int id1 = atoi(cSpiriBot1.GetId().c_str());
      if (id1 < 0) {
         cPosGhost = cSpiriBot1.GetEmbodiedEntity().GetOriginAnchor().Position;
         break;  // Only one ghost
      }
   }

   Real rMaxDiff = 0.0;
   for (CSpace::TMapPerType::iterator it = cSpiribots.begin(); it != cSpiribots.end(); ++it) {
      CSpiriEntity &cSpiriBot = *any_cast<CSpiriEntity*>(it->second);
      int id = atoi(cSpiriBot.GetId().c_str());
      // LOG << id << std::endl;
      if (id >= 0) {
         // Displacement
         CVector3 cPos = cSpiriBot.GetEmbodiedEntity().GetOriginAnchor().Position;
         Real rDiff = SquareDistance(cPos, m_cPrevious[id]);
         //LOG << std::fixed << std::setprecision(6) << rDiff << std::endl;
         if (rDiff > rMaxDiff) {
            rMaxDiff = rDiff;
         }
         m_cPrevious[id] = cPos;
         // Distance to centre: resource consuming, only for testing scenarios
         if (m_counter > 0 || GetSpace().GetSimulationClock() >= (m_maxiterations - m_iterations)) {
            // only if they are the last m_iterations
            for (int j = m_iterations-1; j > 0; --j) {
               m_cHistory[id][j] = m_cHistory[id][j-1];
            }
            m_cHistory[id][0] = Distance(cPos, cPosGhost);
         }
      }
   }


   m_counter++;
   if (rMaxDiff > END_THRESHOLD) {
      m_counter = 0;
   }

   if (m_counter > m_iterations || GetSpace().GetSimulationClock() > m_maxiterations) {

      Real min_drobot = 9e9;
      Real max_drobot = -9e9;
      Real d = 0.0;
      Real min_dcentre = 9e9;
      Real max_dcentre = -9e9;
      Real dc = 0.0;
      int num_d = 0;
      int num_dc = 0;
      std::vector<Real> v1;
      std::vector<CVector3> v2;
      for (CSpace::TMapPerType::iterator it1 = cSpiribots.begin(); it1 != cSpiribots.end(); ++it1) {
         CSpiriEntity &cSpiriBot1 = *any_cast<CSpiriEntity*>(it1->second);

         int id1 = atoi(cSpiriBot1.GetId().c_str());
         if (id1 >= 0) {
            CVector3 cPos1 = cSpiriBot1.GetEmbodiedEntity().GetOriginAnchor().Position;
            v2.push_back(cPos1);

            // Real dct = Distance(cPos1, cPosGhost);
            // LOG << id1 << " : " << std::fixed << std::setprecision(5);
            Real dct = 0.0;
            for (int j = 0; j < m_iterations; ++j) {
               dct += m_cHistory[id1][j];
               // LOG << m_cHistory[id1][j] << " ";
            }
            dct = dct / m_iterations;
            // LOG << ": " << dct << std::endl;
            v1.push_back(dct);
            dc += dct;
            if (dct < min_dcentre) {
               min_dcentre = dct;
            }
            if (dct > max_dcentre) {
               max_dcentre = dct;
            }
            num_dc++;
            for (CSpace::TMapPerType::iterator it2 = cSpiribots.begin(); it2 != cSpiribots.end();
                  ++it2) {
               CSpiriEntity &cSpiriBot2 = *any_cast<CSpiriEntity*>(it2->second);

               int id2 = atoi(cSpiriBot2.GetId().c_str());
               if (id2 >= 0 && id1 != id2) {
                  CVector3 cPos2 = cSpiriBot2.GetEmbodiedEntity().GetOriginAnchor().Position;
                  Real dt = Distance(cPos1, cPos2);
                  d += dt;
                  num_d++;
                  if (dt < min_drobot) {
                     min_drobot = dt;
                  }
                  if (dt > max_drobot) {
                     max_drobot = dt;
                  }
               }
            }

         }
      }
      Real drobot = d / num_d;
      Real dcentre = dc / num_dc;

      Real sd = 0.0;
      for (std::vector<Real>::iterator it1 = v1.begin(); it1 != v1.end(); ++it1) {
         Real dct = *it1;
         sd += (dcentre - dct) * (dcentre - dct);
      }
      sd /= v1.size();
      sd = sqrt(sd);

      LOG << "-----" << std::fixed << std::setprecision(3) << std::endl;
      LOG << drobot << std::endl;
      LOG << min_drobot << std::endl;
      LOG << max_drobot << std::endl;
      LOG << dcentre << std::endl;
      LOG << min_dcentre << std::endl;
      LOG << max_dcentre << std::endl;
      LOG << sd << std::endl;
      LOG << GetSpace().GetSimulationClock() << std::endl;

      for (std::vector<CVector3>::iterator it = v2.begin(); it != v2.end(); ++it) {
         CVector3 v = *it;
         LOG << v.GetX() << "," << v.GetY() << "," << v.GetZ() << std::endl;
      }
      GetSimulator().Terminate();
   }
   if (m_cFailure.size() > 0) {
      char buffer[50];
      CSpace::TMapPerType &cSpiribots = GetSpace().GetEntitiesByType("spiri");
      for (int i = 0; i <= cSpiribots.size(); ++i) {
         int clock = GetSpace().GetSimulationClock();
         if (m_cFailure[i] == clock) {
            sprintf(buffer, "%d", i);
            LOG << clock << " " << buffer << std::endl;
            RemoveEntity(buffer); // Only one can be removed. If two or more, RAB fails, ARGoS bug?
         }
      }
   }
}

void CSpiri3DLoopFunctions::PostExperiment() {

}

REGISTER_LOOP_FUNCTIONS(CSpiri3DLoopFunctions, "spiri3d_loop_functions")
