/**
 * @file <spiri3d.cpp>
 *
 * @author Daniel H. Stolfi
 *
 * ADARS project -- PCOG / SnT / University of Luxembourg
 */

#include "spiri3d.h"

#include <argos3/core/utility/configuration/argos_configuration.h>
#include <argos3/core/simulator/space/space.h>
#include <argos3/core/simulator/simulator.h>
#include "math.h"

/****************************************/
/****************************************/

CSpiri3D::CSpiri3D() :
        m_pcRABAct(NULL), m_pcRABSens(NULL), m_pcPosAct(NULL),
        m_pcPosSens(NULL), m_rThreshold(0.0), m_rDCentre(0.0),
        m_rFCentre(0.0), m_rDCentreMin(0.0), m_rSpeed(SPEED), m_iDebug(0) {
}

/****************************************/
/****************************************/

void CSpiri3D::Init(TConfigurationNode &t_node) {
    m_pcPosAct    = GetActuator<CCI_QuadRotorPositionActuator>("quadrotor_position");
    m_pcPosSens   = GetSensor  <CCI_PositioningSensor        >("positioning"       );
    m_pcRABAct    = GetActuator<CCI_RangeAndBearingActuator  >("range_and_bearing" );
    m_pcRABSens   = GetSensor  <CCI_RangeAndBearingSensor    >("range_and_bearing" );

    int iId =  atoi(CCI_Controller::GetId().c_str());

    GetNodeAttributeOrDefault(t_node, "debug", m_iDebug, m_iDebug);
    GetNodeAttributeOrDefault(t_node, "dcentre", m_rDCentre, m_rDCentre);

    GetNodeAttributeOrDefault(t_node, "threshold", m_rThreshold, m_rThreshold);
    GetNodeAttributeOrDefault(t_node, "fcentre", m_rFCentre, m_rFCentre);
    GetNodeAttributeOrDefault(t_node, "dmincentre", m_rDCentreMin, m_rDCentreMin);
    GetNodeAttributeOrDefault(t_node, "speed", m_rSpeed, m_rSpeed);
    m_rSpeed = m_rSpeed / 100.0;
    // LOG << " " << iId << " " << m_rThreshold << " " << m_rDCentre << " " << m_rFCentre << " " << m_rSpeed << " " << m_rDCentreMin << std::endl;

}

/****************************************/
/****************************************/

void CSpiri3D::ControlStep() {

   CVector3 cPos = m_pcPosSens->GetReading().Position;
   CQuaternion cOri = m_pcPosSens->GetReading().Orientation;
   CRadians cZAngle, cYAngle, cXAngle;
   cOri.ToEulerAngles(cZAngle, cYAngle, cXAngle);
   LOG << std::fixed << std::setprecision(1);

   if (m_iDebug >= 3) {
      LOG << "Position:    " << cPos.GetX() <<
                           "," << cPos.GetY() <<
                           "," << cPos.GetZ() << std::endl;
      LOG << "Orientation: " << ToDegrees(cXAngle.UnsignedNormalize()).GetValue() <<
                           "," << ToDegrees(cYAngle.UnsignedNormalize()).GetValue() <<
                           "," << ToDegrees(cZAngle.UnsignedNormalize()).GetValue() << std::endl;
   }

   bool b_stop = false;
   unsigned uTick = (&CSimulator::GetInstance())->GetSpace().GetSimulationClock();
   std::string sId = CCI_Controller::GetId();
   CByteArray cTxData = CByteArray(3, (UInt8) 0);
   cTxData[0] = (UInt8) atoi(sId.c_str());

   CVector3 cDirection = CVector3(); // TODO: initially random or towards centre
   CVector3 cTemp = CVector3::ZERO;

   /* Get RAB messages */
   const CCI_RangeAndBearingSensor::TReadings& tMsgs = m_pcRABSens->GetReadings();

   // LOG << sId << ": " << n_robots << " " << rThreshold << " " << rFCentre << " " << rDCentreMin << std::endl;

   if(!tMsgs.empty()) {

      for(size_t i = 0; i < tMsgs.size(); ++i) {
         CByteArray cRxData = tMsgs[i].Data;
         Real range = tMsgs[i].Range;
         CRadians hBearing = tMsgs[i].HorizontalBearing;
         CRadians vBearing = tMsgs[i].VerticalBearing;
         if (m_iDebug >= 2) {
            LOG << sId << "> Rx: " << std::fixed << std::setprecision(1) << range <<
                  " H:" << ToDegrees(hBearing).GetValue() << " V:" << ToDegrees(vBearing).GetValue();
            if (m_iDebug >= 4) {
               LOG << " : " << cRxData;
            }
            LOG << std::endl;
         }
         int iSource = cRxData[0];
         int iDest   = cRxData[1];
         char chMsg  = cRxData[2];

         Real d;
         Real rep = 1.0;
         if (iSource >= 200) {  // int -> uint8: Source is a Ghost
            d = m_rDCentre;
            if (range < m_rDCentreMin) { // centre threshold
               rep = m_rFCentre; // extra repelling force
            }
            if (range < (m_rDCentre / 3.0)) {  // not too close to a Ghost
               d = 10 * range; //b_stop = true;
               LOG << sId << "> STOPPED: CENTRE " << std::fixed << std::setprecision(3) << range << std::endl;
            }
         } else {
            // TODO: colision avoidance
            d = m_rThreshold;
            if (range < COLLISION_DISTANCE) {
               d = 0;
            }
            if (range < COLLISION_CRITICAL) { // not too close to another EPuck2
               b_stop = true;
               LOGERR << sId << "> STOPPED: EPUCK2 " << std::fixed << std::setprecision(3) << range << std::endl;
               CSimulator::GetInstance().Terminate();
            }
         }

         cTemp.FromSphericalCoords(rep * (range - d), CRadians::PI_OVER_TWO - vBearing, hBearing);
         cDirection += cTemp;

      }
   }
   cDirection /= tMsgs.size() * 200.0;

   if (!b_stop) {
      Real fAlpha = 1.0;
      if (cDirection.Length() < 5.0) {
         fAlpha = cDirection.Length() * cDirection.Length() / 25.0f;
      }
      if (cDirection.Length() > 1.0) {
         cDirection.Normalize();
      }

      // LOG << sId << " " << std::fixed << std::setprecision(3) << fAlpha << " " << cDirection.Length() << " " << (cDirection * m_rSpeed * fAlpha).Length() << std::endl;
      m_pcPosAct->SetRelativePosition(cDirection * m_rSpeed * fAlpha);

   } else {
      m_pcPosAct->SetRelativePosition(CVector3::ZERO);
      if (m_iDebug >= 1) {
         LOG << sId << "> " << "STOPPED" << std::endl;
      }
   }

   cTxData[1] = (UInt8) 0;
   cTxData[2] = (UInt8) '-';
   m_pcRABAct->SetData(cTxData);
   if (m_iDebug >= 3) {
      LOG << sId << "> Tx:" << cTxData;
   }

}

/****************************************/
/****************************************/

REGISTER_CONTROLLER(CSpiri3D, "spiri3d_controller")
