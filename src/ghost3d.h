/**
 * @file <ghost3d.h>
 *
 * @author Daniel H. Stolfi
 *
 * ADARS project -- PCOG / SnT / University of Luxembourg
 */

#ifndef GHOST3D_H
#define GHOST3D_H

#include <argos3/core/control_interface/ci_controller.h>
#include <argos3/plugins/robots/generic/control_interface/ci_quadrotor_position_actuator.h>
#include <argos3/plugins/robots/generic/control_interface/ci_positioning_sensor.h>
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_actuator.h>
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_sensor.h>

#include <argos3/core/utility/math/vector3.h>
#include <argos3/core/simulator/simulator.h>

using namespace argos;

const Real COLLISION_DISTANCE = 30.0;
const Real COLLISION_CRITICAL = 10.0;
const int MAX_ROBOTS = 30;

class CGhost3D: public CCI_Controller {

    public:

        CGhost3D();

        virtual ~CGhost3D() {}

        virtual void Init(TConfigurationNode &t_node);

        virtual void ControlStep();

        virtual void Destroy() {}

    private:
        struct SPlan {
           CVector3 Destination;
           unsigned Begin;
           Real     Speed;

           SPlan() : Destination(CVector3::ZERO), Begin(0), Speed(0.0) {}
           SPlan(const CVector3& c_destinaton,
                 const unsigned c_begin,
                 const Real c_speed): Destination(c_destinaton), Begin(c_begin), Speed(c_speed) {}
        };

        std::vector<SPlan> m_cPlan;

        CCI_QuadRotorPositionActuator* m_pcPosAct;
        CCI_PositioningSensor* m_pcPosSens;
        CCI_RangeAndBearingActuator* m_pcRABAct;
        CCI_RangeAndBearingSensor* m_pcRABSens;

        int m_iDebug, m_iHAngle, m_iVAngle;
        SPlan m_cCurrentPlan;
        int m_iPlan;
        bool m_bOn;

        SPlan GetPlan(const std::string str);
};

#endif
