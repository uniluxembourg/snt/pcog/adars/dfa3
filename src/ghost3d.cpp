/**
 * @file <ghost3d.cpp>
 *
 * @author Daniel H. Stolfi
 *
 * ADARS project -- PCOG / SnT / University of Luxembourg
 */

#include "ghost3d.h"

#include <argos3/core/utility/configuration/argos_configuration.h>
#include <argos3/core/simulator/space/space.h>
#include <argos3/core/simulator/simulator.h>
#include "math.h"

/****************************************/
/****************************************/

CGhost3D::CGhost3D() :
        m_pcRABAct(NULL), m_pcRABSens(NULL), m_pcPosAct(NULL),
        m_pcPosSens(NULL), m_iDebug(0),
        m_iHAngle(0), m_iVAngle(0), m_iPlan(-1), m_bOn(true) {
}

/****************************************/
/****************************************/

void CGhost3D::Init(TConfigurationNode &t_node) {
    m_pcPosAct    = GetActuator<CCI_QuadRotorPositionActuator>("quadrotor_position");
    m_pcPosSens   = GetSensor  <CCI_PositioningSensor        >("positioning"       );
    m_pcRABAct    = GetActuator<CCI_RangeAndBearingActuator  >("range_and_bearing" );
    m_pcRABSens   = GetSensor  <CCI_RangeAndBearingSensor    >("range_and_bearing" );

    std::string sPlan = "";
    GetNodeAttributeOrDefault(t_node, "debug", m_iDebug, m_iDebug);
    GetNodeAttributeOrDefault(t_node, "plan", sPlan, sPlan);

    m_cCurrentPlan = SPlan();
    if (sPlan != "") {
       std::size_t found = sPlan.find_first_of(":");
       while (found != std::string::npos) {
          std::string step = sPlan.substr(0, found);
          m_cPlan.push_back(GetPlan(step));
          sPlan = sPlan.substr(found+1);
          found = sPlan.find_first_of(":");
       }
       m_cPlan.push_back(GetPlan(sPlan));
       m_iPlan = -1;
       m_cCurrentPlan.Begin = 0;
    }
}

/****************************************/
/****************************************/

void CGhost3D::ControlStep() {

   CVector3 cPos = m_pcPosSens->GetReading().Position;
   CQuaternion cOri = m_pcPosSens->GetReading().Orientation;
   CRadians cZAngle, cYAngle, cXAngle;
   cOri.ToEulerAngles(cZAngle, cYAngle, cXAngle);
   LOG << std::fixed << std::setprecision(1);

   if (m_iDebug >= 3) {
      LOG << "Position:    " << cPos.GetX() <<
                           "," << cPos.GetY() <<
                           "," << cPos.GetZ() << std::endl;
      LOG << "Orientation: " << ToDegrees(cXAngle.UnsignedNormalize()).GetValue() <<
                           "," << ToDegrees(cYAngle.UnsignedNormalize()).GetValue() <<
                           "," << ToDegrees(cZAngle.UnsignedNormalize()).GetValue() << std::endl;
   }

   bool b_stop = false;
   unsigned uTick = (&CSimulator::GetInstance())->GetSpace().GetSimulationClock();
   std::string sId = CCI_Controller::GetId();
   CByteArray cTxData = CByteArray(3, (UInt8) 0);
   cTxData[0] = (UInt8) atoi(sId.c_str());

   if ((m_iPlan + 1) < m_cPlan.size() && uTick >= m_cPlan[m_iPlan + 1].Begin) {
      m_iPlan++;
      m_cCurrentPlan = m_cPlan[m_iPlan];

      if (m_iDebug >= 2) {
         if (m_iPlan >= 0) {
            LOG << "Plan: " << m_cCurrentPlan.Begin << " " << m_cCurrentPlan.Destination << std::endl;
         }
      }
   }
   if (m_iPlan >= 0) {
      CVector3 cDirection = m_cCurrentPlan.Destination - cPos;
      m_pcPosAct->SetRelativePosition(cDirection.Normalize() * m_cCurrentPlan.Speed);
   } else {
      m_pcPosAct->SetRelativePosition(CVector3::ZERO);
   }

   cTxData[1] = (UInt8) 0;
   cTxData[2] = (UInt8) '-';
   m_pcRABAct->SetData(cTxData);
   if (m_iDebug >= 3) {
      LOG << sId << "> Tx:" << cTxData;
   }

}


/****************************************/
/****************************************/

CGhost3D::SPlan CGhost3D::GetPlan(const std::string str) {

   std::string step = str;
   // begin time
   std::size_t pos = step.find_first_of(",");
   unsigned uBegin = atoi(step.substr(0, pos).c_str());
   step = step.substr(pos+1);
   // speed
   pos = step.find_first_of(",");
   Real rSpeed = atof(step.substr(0, pos).c_str());
   step = step.substr(pos+1);
   // X
   pos = step.find_first_of(",");
   Real x = atof(step.substr(0, pos).c_str());
   step = step.substr(pos+1);
   // Y
   pos = step.find_first_of(",");
   Real y = atof(step.substr(0, pos).c_str());
   step = step.substr(pos+1);
   // Z
   pos = step.find_first_of(",");
   Real z = atof(step.substr(0, pos).c_str());
   return SPlan(CVector3(x, y, z), uBegin, rSpeed);
}

/****************************************/
/****************************************/

REGISTER_CONTROLLER(CGhost3D, "ghost3d_controller")
