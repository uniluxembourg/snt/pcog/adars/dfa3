/**
 * @file <spiri3d_loop_functions.h>
 *
 * @author Daniel H. Stolfi
 *
 * ADARS project -- PCOG / SnT / University of Luxembourg
 */

#ifndef SPIRI3D_H
#define SPIRI3D_H

#include <argos3/core/control_interface/ci_controller.h>
#include <argos3/plugins/robots/generic/control_interface/ci_quadrotor_position_actuator.h>
#include <argos3/plugins/robots/generic/control_interface/ci_positioning_sensor.h>
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_actuator.h>
#include <argos3/plugins/robots/generic/control_interface/ci_range_and_bearing_sensor.h>

#include <argos3/core/utility/math/vector3.h>
#include <argos3/core/simulator/simulator.h>

using namespace argos;

const Real SPEED = 1.00;
const Real COLLISION_DISTANCE = 30.0;
const Real COLLISION_CRITICAL = 10.0;
const int MAX_ROBOTS = 30;

class CSpiri3D: public CCI_Controller {

    public:

        CSpiri3D();

        virtual ~CSpiri3D() {}

        virtual void Init(TConfigurationNode &t_node);

        virtual void ControlStep();

        virtual void Destroy() {}

    private:
        CCI_QuadRotorPositionActuator* m_pcPosAct;
        CCI_PositioningSensor* m_pcPosSens;
        CCI_RangeAndBearingActuator* m_pcRABAct;
        CCI_RangeAndBearingSensor* m_pcRABSens;

        Real m_rThreshold, m_rDCentre, m_rFCentre, m_rSpeed, m_rDCentreMin;
        int m_iDebug;

};

#endif
