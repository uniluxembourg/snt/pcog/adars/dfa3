# -*- coding: utf-8 -*-
# author: Daniel H. Stolfi"""
# DFA3 evaluator
# ADARS project -- PCOG / SnT / University of Luxembourg
#
import os
import subprocess
import tempfile
import multiprocessing as mp
import time

def simulate(threshold, dmincentre, fcentre, speed, dcentre, iterations, maxiterations, path, visualise=False, delete=True, traces=False):
    """
    Simulation.

    :param int threshold: the distance threshold between robots
    :param int dmincentre: the minimum distance to the centre
    :param int fcentre: the repelling centre force intensity (x10)
    :param int speed: the speed of robots (x100)
    :param int dcentre: the distance to the centre
    :param int iterations: the number of iterations for termination
    :param int maxiterations: the maximum number of iterations
    :param str path: the coplete path of the ARGoS xml file
    :param bool visualise: `True` for visualisation
    :param bool delete: `True` to delete the ARGoS temp file
    :param bool traces: `True` to generate robots' traces
    """

    def list2str(plist):
       s = ""
       for v in plist:
           s += "{:},".format(v)
       return s[:-1]

    temp_file = "/tmp/temp_" + next(tempfile._get_candidate_names()) + ".argos"

    with open(path, 'r') as in_f:
       with open(temp_file, 'w') as out_f:
          for line in in_f:
             if line.find('THRESHOLD') != -1 and line.find('DCENTRE') != -1 and \
                   line.find('DMINCENTRE') != -1 and line.find('FCENTRE') != -1 and \
                   line.find('SPEED_VALUE') != -1:
                line = line.replace('THRESHOLD', str(threshold))
                line = line.replace('DCENTRE', str(dcentre))
                line = line.replace('DMINCENTRE', str(dmincentre))
                line = line.replace('FCENTRE', str(fcentre))
                line = line.replace('SPEED_VALUE', str(speed))
             elif line.find('ITERATIONS') != -1 and line.find('MAXTERATIONS') != -1:
                line = line.replace('ITERATIONS', str(iterations))
                line = line.replace('MAXTERATIONS', str(maxiterations))
             elif visualise and line.find('<visualization />') != -1:
                line = line.replace('<visualization />', '<visualization><qt-opengl><camera><placements>' +
                                    '<placement index="0" position="55.2628,-0.418411,55.5054" ' +
                                    'look_at="54.5069,-0.418411,54.8508" up="0,0,1" lens_focal_length="65" />' +
                                    '<placement index="1" position="0.10791,-0.0458422,56.3917" ' +
                                    'look_at="0.10791,-0.0458422,55.3917" up="-0.999988,-0.00492688,0" lens_focal_length="65" />' +
                                    '</placements></camera></qt-opengl></visualization>')
             elif traces and line.find('label="swarm_loop_functions"') != -1:
                line += '<debug trace="true" />\n'

             out_f.write(line)

    result = subprocess.run(["argos3", "-n", "-c", temp_file], stderr=subprocess.PIPE,
                            stdout=subprocess.PIPE, universal_newlines=True)
    if result.returncode != 0:
        raise Exception("Error executing ", result.returncode, result.args, result.stdout, result.stderr)
    if delete:
       os.remove(temp_file)
    out = str(result.stdout).split('\n')
    if traces:
       return out
    i = 0
    for o in out:
       if o == "-----":
          return out[i+1:-1]
       i += 1

def evaluate(seeds, robots, threshold, dmincentre, fcentre, speed, dcentre, iterations, maxiterations, cores, visualisation=False, delete=True):
    """
    Evaluation.

    :param list[int] seeds: the seed list to evaluate
    :param int robots: the number of robots
    :param int threshold: the distance threshold between robots (x100)
    :param int dmincentre: the minimum distance to the centre
    :param int fcentre: the centre repelling force intensity (x10)
    :param int speed: the speed of robots (%)
    :param int dcentre: the distance to the centre (x10)
    :param int iterations: the number of iterations for termination
    :param int maxiterations: the maximum number of iterations
    :param int cores: the parellelism level
    :param bool visualisation: `True` for visualisation
    :param bool delete: `True` to delete the ARGoS temp file
    """
    files = list()
    directory = "experiments"
    for s in seeds:
       filename = "{:03}_{:02}.argos".format(s, robots)
       config_file = os.path.join(directory, filename)
       files.append(config_file)
    if cores > 1:
       thresholds = [threshold] * len(seeds)
       dcentres = [dcentre] * len(seeds)
       dmincentres = [dmincentre] * len(seeds)
       fcentres = [fcentre] * len(seeds)
       deletes = [delete] * len(seeds)
       speeds = [speed] * len(seeds)
       iterationss = [iterations] * len(seeds)
       maxiterationss = [maxiterations] * len(seeds)
       visualisations = [visualisation] * len(seeds)

       with mp.Pool(cores) as p:
          params = list(zip(thresholds, dmincentres, fcentres, speeds, dcentres, iterationss, maxiterationss, files, visualisations, deletes))
          rlist = p.starmap(simulate, params)
    else:
       rlist = list()
       for filename in files:
          rlist.append(simulate(threshold, dmincentre, fcentre, speed, dcentre, iterations, maxiterations, filename, visualisation, delete))

    f = 0.0;
    for r in rlist:
       if r is None:
          return 50
       else:
          w = 0
          m_2 = maxiterations / 2
          n_i = int(r[7])
          if n_i > m_2:
             w = (n_i - m_2) / 200
          f += abs(float(r[4]) - dcentre/100.0) + abs(float(r[5]) - dcentre/100.0) + abs(2.0 * dcentre/100.0 - float(r[1])) + w
    return f / len(rlist)

def surrogate(seed, robots, threshold, dmincentre, fcentre, speed, dcentre, iterations, maxiterations, cf = "lin"):
    """
    Evaluation.

    :param seed: the seed list to evaluate
    :param int robots: the number of robots
    :param int threshold: the distance threshold between robots (x100)
    :param int dmincentre: the minimum distance to the centre
    :param int fcentre: the centre repelling force intensity (x10)
    :param int speed: the speed of robots (%)
    :param int dcentre: the distance to the centre (x10)
    :param int iterations: the number of iterations for termination
    :param int maxiterations: the maximum number of iterations
    :param str cf: The covariance function. `lin` by default
    """
    if cf == "cell":
       scenario =  "./scenarios/{:03d}_{:02d}.ini".format(seed, robots)
       result = subprocess.run(["./main", scenario, str(threshold),
                                str(dmincentre), str(fcentre), str(speed),
                                str(dcentre), str(iterations), "0"], stderr=subprocess.PIPE,
                            stdout=subprocess.PIPE, universal_newlines=True)
       out = str(result.stdout).split('\n')
       return abs(float(out[4]) - dcentre/100.0) + abs(float(out[5]) - dcentre/100.0) + abs(2.0 * dcentre/100.0 - float(out[1]))

    else:
       if cf == "ann":
          params = "nn{:02d} {:} {:} {:} {:}".format(robots, threshold, dmincentre, fcentre, speed)
       else:
          params = "gp{:02d}-{:} {:} {:} {:} {:}".format(robots, cf, threshold, dmincentre, fcentre, speed)
       result = subprocess.run(["Rscript", "--vanilla ./R/evaluate.R "+ params], stderr=subprocess.PIPE,
                               stdout=subprocess.PIPE, universal_newlines=True)
       return float(result.stdout)

def metrics(seeds, robots, threshold, dmincentre, fcentre, speed, dcentre, iterations, maxiterations, visualisation=False, delete=True, failures=list()):
    directory = "experiments"
    print("Time,Drobots*,Dcentre*,Dmincentre*,Dmaxcentre*,SD,EndTime,Fitness")
    for s in seeds:
       filename = "{:03}_{:02}.argos".format(s, robots)
       config_file = os.path.join(directory, filename)
       begin_time = time.time()
       r = simulate(threshold, dmincentre, fcentre, speed, dcentre, iterations, maxiterations, config_file, visualisation, delete)
       if r is None:
           print("{:.3f},-1,-1,-1,-1,-1,-1,50.0".format(time.time() - begin_time))
       else:
           w = 0
           m_2 = maxiterations / 2
           n_i = int(r[7])
           if n_i > m_2:
              w = (n_i - m_2) / 200
           f = abs(float(r[4]) - dcentre/100.0) + abs(float(r[5]) - dcentre/100.0) + abs(2.0 * dcentre/100.0 - float(r[1])) + w

           print("{:.3f},{:.3f},{:.3f},{:.3f},{:.3f},{:.3f},{:4d},{:.3f}".format(
                 time.time() - begin_time, float(r[0]),float(r[3]),
                 float(r[4]),float(r[5]),float(r[6]),int(r[7]), f))

training = [433, 419, 277, 499, 227, 151,  73, 139,  61, 239]

testing = [521, 29,  563, 569, 571, 577, 587, 593, 599, 601,
           607, 613, 617, 619, 631, 641, 643, 647, 653, 659,
           661, 673, 677, 683, 691, 701, 709, 719, 727, 733]

if __name__ == "__main__":

   metrics(testing, 3, 871, 849, 112, 157, 300, 300, 2000)

   metrics(testing, 5, 889, 896, 193, 199, 300, 300, 2000)

   metrics(testing, 10, 499, 900, 436, 184, 300, 300, 2000)

   metrics(testing, 15, 1133, 979, 538, 167, 400, 300, 2000)

   metrics(testing, 30, 623, 524, 1489, 9, 500, 300, 2000)

