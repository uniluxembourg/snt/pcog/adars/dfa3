# Distributed Formation Algorithm 3

## Compiling the code

Make sure you have ARGoS >= 3.0.0-beta52 installed!

Commands:

```shell
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

## Evaluation

```shell
python3 evaluator.py
```

## Results

Calculated results are in /results/ folder


## Surrogate Model Training

```shell
cd R
Rscript --vanilla training.R
```

## Surrogate Model Testing

```shell
cd R
Rscript --vanilla testing.R
```
